# IFB FAIR bioinfo training - SnakeMake workflow

Use during the [IFB FAIR bioinfo training](https://ifb-elixirfr.github.io/IFB-FAIR-bioinfo-training/index.html#home)

## Dataset

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3997237.svg)](https://doi.org/10.5281/zenodo.3997237)

```bash
wget -O FAIR_Bioinfo_data.tar.gz https://zenodo.org/record/3997237/files/FAIR_Bioinfo_data.tar.gz?download=1
```

## Usage

This training workflow is link to this Notebook : [Workflow Initiation](https://gitlab.com/ifb-elixirfr/training/notebooks/workflow_initiation)

⚠️ Compatible with Snakemake 7.x

### Executor

#### Local

```
snakemake --use-singularity
```

### Manage dependencies

Snakemake documentation: [Integrated Package Management](https://snakemake.readthedocs.io/en/stable/snakefiles/deployment.html#integrated-package-management)

#### Conda

```bash
snakemake --slurm --profile ./profile/slurm --use-conda 
```

#### Singularity

```bash
snakemake --slurm --profile ./profile/slurm --use-singularity
```

#### Env module

```bash
snakemake --slurm --profile ./profile/slurm --use-envmodule
```


